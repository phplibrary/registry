#SINEVIA REGISTRY

The Registry class provides a safe implementation of global registry.
This acts like an alternative of the the global variable $GLOBALS, which
references all variables available in global scope, but can be already
being used by anothe library.

# Installation #

```
#!json

   "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/phplibrary/registry.git"
        }
    ],
    "require": {
        "sinevia/phplibrary/registry": "dev-master"
    },
```

# How to Use? #
<code>
Registry::set('admin_email','admin@domain.com');
 
if (Registry::has('admin_email')) {
     echo Registry::get('admin_email');
}

Registry::remove('admin_email');
</code>